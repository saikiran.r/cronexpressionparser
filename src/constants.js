const GENERIC_ERROR_MESSAGE = "Invalid Input!! This is not supported. Please enter a valid cron expression";
const cronFields = [{
  start: 0,
  end: 59,
  displayText: "minutes"
}, {
  start: 0,
  end: 23,
  displayText: "hours"
}, {
  start: 1,
  end: 31,
  displayText: "day of month"
}, {
  start: 1,
  end: 12,
  displayText: "month"
}, {
  start: 0,
  end: 6,
  displayText: "day of week"
}, {
  start: 0,
  end: 0,
  displayText: "command"
}];

module.exports = {
  GENERIC_ERROR_MESSAGE: GENERIC_ERROR_MESSAGE,
  cronFields: cronFields
}

// exports.GENERIC_ERROR_MESSAGE = GENERIC_ERROR_MESSAGE;
// exports.cronFields = cronFields;