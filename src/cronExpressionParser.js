//import constants
var GENERIC_ERROR_MESSAGE = require('./constants').GENERIC_ERROR_MESSAGE;
var cronFields = require('./constants').cronFields;

function cronExpressionParser(expression){
  if(!expression){
    return GENERIC_ERROR_MESSAGE;
  }
  try{
    const expressionList = expression.split(' ');
    if(expressionList.length !== 6){
      console.error(GENERIC_ERROR_MESSAGE);
      return GENERIC_ERROR_MESSAGE;
    }

    const cronFieldsParsedList = [];
    expressionList.forEach((exp, index) => {
      //try catch to throw an error
      cronFieldsParsedList.push(evaluateExpression(exp, index));
    });
    if(cronFieldsParsedList.length !== 6){
      console.error(GENERIC_ERROR_MESSAGE);
      return GENERIC_ERROR_MESSAGE;
    }
    let output = '\n';
    cronFieldsParsedList.forEach((parsedStr, index) => {
      if(!parsedStr){
        throwParseError(index, GENERIC_ERROR_MESSAGE);
      }
      output += cronFields[index].displayText.padEnd(14) + parsedStr.join(' ') + '\n'
    })
    return output;
  } catch(error){
    return error.message;
  }
}

function evaluateExpression(expression, cronFieldIndex){
  
  if(!expression){
    throwParseError(cronFieldIndex, message);
  }
  if(cronFields[cronFieldIndex].displayText === "command"){
    return [expression];
  }
  
  let parsedExpression;
  if(expression === '*'){
    parsedExpression = rangeExpression(cronFields[cronFieldIndex].start+'-'+cronFields[cronFieldIndex].end, cronFieldIndex);
  } else if(expression.length <= 2){
    parsedExpression = [simpleNumberExpression(expression, cronFieldIndex)];
  } else if(expression.indexOf(',') != -1){
    parsedExpression = listExpression(expression, cronFieldIndex);
  } else if(expression.indexOf('/') != -1){
    parsedExpression = intervalExpression(expression, cronFieldIndex);
  } else if(expression.indexOf('-') != -1){
    parsedExpression = rangeExpression(expression, cronFieldIndex);
  } else {
    throwParseError(cronFieldIndex, GENERIC_ERROR_MESSAGE);
  }
  return parsedExpression;
}

function rangeExpression(expression, cronFieldIndex, incrementInterval = 1){
  let range = expression.split('-');
  if(range.length !== 2){
    throwParseError(cronFieldIndex);
  }
  
  const [start, end] = [parseInt(range[0]), parseInt(range[1])];
  if(start >= cronFields[cronFieldIndex].start && end <= cronFields[cronFieldIndex].end){
    const timeList = [];
    for(let i=start; i <= end;){
      timeList.push(i);
      i += incrementInterval;
    }
    return timeList;
  } else {
    throwParseError(cronFieldIndex);
  }
}

function intervalExpression(expression, cronFieldIndex){
  let interval = expression.split('/');
  const secondParam = parseInt(interval[1]);
  if(interval.length !== 2 || interval[1].length > 2 || !secondParam){
    throwParseError(cronFieldIndex);
  }
  if(interval[0] === '*'){
    return rangeExpression(cronFields[cronFieldIndex].start+'-'+cronFields[cronFieldIndex].end, cronFieldIndex, secondParam);
  } else if(interval[0].length <= 2){
    const firstParam = parseInt(interval[0]);
    if(!firstParam){
      throwParseError(cronFieldIndex);
    }
    return rangeExpression(firstParam+'-'+cronFields[cronFieldIndex].end, cronFieldIndex, secondParam);
  } else if(interval[0].indexOf('-') != -1){
    return rangeExpression(interval[0], cronFieldIndex, secondParam);
  } else {
    throwParseError(cronFieldIndex);
  }
}

function listExpression(expression, cronFieldIndex){
  let list = expression.split(',');
  if(list.length < 2){
    throwParseError(cronFieldIndex);
  }
  let setOfTimes = new Set();
  list.forEach(subExp => {
    if(subExp.length <= 2){
      setOfTimes.add(simpleNumberExpression(subExp, cronFieldIndex));
    } else if(subExp.indexOf('/') != -1){
      setOfTimes = mergeArrayInSet(setOfTimes, intervalExpression(subExp, cronFieldIndex));
    } else if(subExp.indexOf('-') != -1){
      setOfTimes = mergeArrayInSet(setOfTimes, rangeExpression(subExp, cronFieldIndex));
    } else {
      throwParseError(cronFieldIndex);
    }
  });
  return [...setOfTimes].sort((a,b) => a-b);
}

function simpleNumberExpression(expression, cronFieldIndex){
  const number = parseInt(expression);
  if(number >= cronFields[cronFieldIndex].start && number <= cronFields[cronFieldIndex].end){
    return number;
  } else {
    throwParseError(cronFieldIndex);
  }
}

function throwParseError(cronFieldIndex, message = ''){
  if(!message){
    console.error("Invalid Input!! %s field is not correct in the given cron expression", cronFields[cronFieldIndex].displayText);
    throw new Error("Invalid Input!! " + cronFields[cronFieldIndex].displayText + " field is not correct in the given cron expression");
  } else {
    console.error(message);
    throw new Error(message);
  }
}

function mergeArrayInSet(setA, arrayA){
  arrayA.forEach(item => setA.add(item));
  return setA;
}

exports.default = cronExpressionParser; 