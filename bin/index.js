#!/usr/bin/env node

const yargs = require("yargs");
const cronExpressionParser = require("../src/cronExpressionParser").default;

const usage = "\nUsage: cron-expression-parser <expression>";
const options = yargs
      .usage(usage)                                                                                
      .help(true)
      .argv;

const expression = options["_"].join(' ');

console.log(cronExpressionParser(expression));