const cronExpressionParser = require('../src/cronExpressionParser').default;
var chai = require('chai');
var expect = chai.expect; 
var assert = require('chai').assert;
var should = require('chai').should();

describe('cronExpressionParser', function() {
  it('test cronExpressionParser using valid expression', function() {
    var output = cronExpressionParser('*/15 0 1,15 * 1-5/21 /usr/bin/find');
    assert.typeOf(output, 'string', 'output is a string');
    
    var outputSplit = output.split('\n')
    assert.lengthOf(outputSplit, 8, 'Output has parsed the expression successfully. It contains all fields in output');
    
    assert.equal(outputSplit[1], 'minutes       0 15 30 45', 'first row has `minutes` with value `0 15 30 45`');
    assert.equal(outputSplit[2], 'hours         0', '2nd row has `hours` with value `0`');
    assert.equal(outputSplit[3], 'day of month  1 15', '3rd row has ``day of month` with value 1 15`');
    assert.equal(outputSplit[4], 'month         1 2 3 4 5 6 7 8 9 10 11 12', '4th row has `month` with value `1 2 3 4 5 6 7 8 9 10 11 12`');
    assert.equal(outputSplit[5], 'day of week   1', '5th row has `day of week` with value `1`');
    assert.equal(outputSplit[6], 'command       /usr/bin/find', '6th row has `command` with value `/usr/bin/find`');
  });

  it('test cronExpressionParser using invalid expression', function() {
    var output = cronExpressionParser('*/15 0 1,15 */* 1-5/21 /usr/bin/find');
    assert.typeOf(output, 'string', 'output is a string');
    
    var outputSplit = output.split('\n')
    outputSplit.length.should.not.equal(8);
    
    assert.equal(output, 'Invalid Input!! month field is not correct in the given cron expression');
  });

}); 
