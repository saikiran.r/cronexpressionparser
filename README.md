# cronExpressionParser


#How to run

**1. Quick and easy way** 
1. Copy the code from `assets/build.js` file and past it in the `broswer developer console` and click enter.
2. call the function `cronExpressionParser` by passing the cron expression as a param

`cronExpressionParser('*/15 0 1,15 * 1-5 /usr/bin/find')`
output: 
"
minutes       0 15 30 45
hours         0
day of month  1 15
month         1 2 3 4 5 6 7 8 9 10 11 12
day of Week   1 2 3 4 5
command       /usr/bin/find
"

`cronExpressionParser('*/15 0 1,15 */* 1-5/21 /usr/bin/find');`
output:
"Invalid Input!! month field is not correct in the given cron expression"



**2. To run the module in local using node.js**
Pre-requisites: we would need to have a node.js installed with v10+ to run this
1. Clone the repo using `git clone <clone url>`
2. Go to project folder 
3. Install all node dependencies `npm install`.
3. Install the module globally `npm install -g .`
4. Run the module to use and test
eg: `cron-expression-parser */15 0 1,15 */* 1-5/21 /usr/bin/find`



# Todo:
1. To validate the simple number expressions, we are Checking the length that needs to be less or equal to 2. This doesn't work for numbers with 0003. So, this needs to be changed.
2. We are using parseInt which is not validating the expression properly. This would convert '3-' into 3 in javascript. Would like to use own parser by validating the string first.
3. Doesn't accept month/day of week in string like JAN/SUN. The parser code only accepts numbers i.e 1/2. 
4. Used sort function. Would use my own sort technique that could increase the performance.
5. Modulerise/refactor the code i.e move those functions into different files for maintainability.
6. Add more unit tests to test each function and code explicitly.
7. Add built scripts to complile the src files and generate build file autoatically.
8. All months show 31 days. This is not creating any problem at the moment but could be improved.

# Algorithm:
1. split down the string using delimiter ' '.
2. If the length is 6, then it is has all cron inputs. Proceed with next steps. Orelse, throw an error
3. check for patterns: *, number, ',', '-', and '/'
minute -  0-59
hour - 0-23
day - 1-31
month - 1-12  
day - 0-6

**Conditions:**
When there is only single char and * = add all numbers,
There there is a valid number = add the number, 
When the expression is big, 
Firstly, split by ','. It can have any length
Secondly, split by '/'. Should have only length 2. 1st part could be * or number or having '-', 2nd part should have only valid numbers
Thirdly, split by '-'. Should have only length 2 and should contain only valid numbers
